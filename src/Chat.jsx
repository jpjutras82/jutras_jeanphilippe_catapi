import React from "react";

const Chat = ({ url, id }) => {
  return (
    <article className="card col-md-6 col-lg-3 mx-1 my-2 pt-3 text-center">
      <a href={url} className="nounderline">
        <img src={url} className="card-img-top" alt="" />
        <div className="card-body">
          <h5 className="text-warning">Chat de l'espace</h5>
          <p className="text-muted">(id: {id})</p>
        </div>
      </a>
    </article>
  );
};

export default Chat;
