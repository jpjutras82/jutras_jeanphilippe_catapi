import React, { useEffect, useState } from "react";
import Chat from "./Chat";
import "./App.css";
import ReactPaginate from "react-paginate";

// APPEL DE L'API + DÉCLARATION DES CLÉS
const App = () => {
  // STATES: Déclaration des états pour stocker les infos obtenues de CAT API.
  const [chats, setChats] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(0);
  const [limit, setLimit] = useState(8);

  // GET API HOMEPAGE + GESTION DU NOMBRE DE CHATS (8 ITEMS PAR PAGE)
  const catApiUrl = `https://api.thecatapi.com/v1/images/search?limit=${limit}&page=${currentPage}&order=desc`;

  // GET HOMEPAGE
  const GetCatsHomepage = async () => {
    const response = await fetch(catApiUrl, {
      method: "GET",
      headers: {
        "x-api-key": `9f5aaec0-dbe7-47c6-b109-c142fedfccf4`,
      },
    });
    setPageCount(response.headers.get("pagination-count"));
    const dataCat = await response.json();
    setChats(dataCat);
  };

  // EXÉCUTION DE useEffect (async await utilisé):
  useEffect(() => {
    GetCatsHomepage();
  }, []);

  // --------------------------------------------------------------------------------------------------------------

  // GET API SUR CHANGEMENT DE PAGE
  const handlePageClick = async (data) => {
    setCurrentPage(data.selected + 1);
    const selectedPage = await getPageChange(currentPage);
    setChats(selectedPage);
    console.log(selectedPage);
    getPageChange();
  };

  const getPageChange = async (currentPage) => {
    const res = await fetch(catApiUrl, {
      method: "GET",
      headers: {
        "x-api-key": `9f5aaec0-dbe7-47c6-b109-c142fedfccf4`,
      },
    });
    const data = await res.json();
    return data;
  };

  // --------------------------------------------------------------------------------------------------------------
  // AFFICHAGE API CHATS 9 PROPS: Déclaration des props
  return (
    <div className="App">
      {/* BARRE DE NAVIGATION (PAGINATION) */}
      <div>
        <ReactPaginate
          previousLabel={"previous"} // page précédente
          nextLabel={"next"} //page suivante
          breakLabel={"..."} //break label
          pageCount={pageCount} //nb pages totale (remplacer par variable)
          pageRangeDisplayed={4} //nb pages affichés avant
          marginPagesDisplayed={4} //nb pages affichés après
          onPageChange={handlePageClick}
          containerClassName={"pagination justify-content-center"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          activeClassName={"active"}
        />
      </div>
      <div>
        <h5 className="text-danger text-center">Bogue: Recliquer sur une autre page pour afficher la dernière page svp!</h5>
      </div>
      {/* CARTE DES ITEMS "CHAT" */}
      <div className="container-fluid">
        <div className="row">
          <div className="chats">
            {chats.map((chat) => (
              <Chat key={chat.id} id={chat.id} url={chat.url} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
